package exercice1_2;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
import org.apache.commons.codec.binary.Base64;

/*
 * La Classe principale
 */  

public class Main {
	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
	    try {
	    	/* Charger l'image 
	    	 */
	        File image = new File("/Users/Oub/Desktop/foadNfa035Exercice1_2/src/foadNfa035Exercie1_2/petite_image_2.png");
	       
	        /*
	         * On crée une instance de ImageByteArrayFrame ,,,??
	         */
	        ImageByteArrayFrame media = new ImageByteArrayFrame(new ByteArrayOutputStream());

	        //============================================= Sérialisation ============================================================
	        
	        /*
	         * creation de l'instance de serializer
	         */
	        ImageStreamingSerializer serializer = new ImageSerializerBase64StreamingImpl();
	        
	        /*
	         * serialisation de l'image: IMAGE->media 
	         */
	        serializer.serialize(media, image);
	        
	        /*
	         * convertir le tableau de bites en String
	         */
	        String encodedImage = media.getEncodedImageOutput().toString();
	        
	        /*
	         *Affichage de l'image encodée
	         */
	        System.out.println(encodedImage + "\n");

	        // ============================================= Désérialisation =========================================================
	        
	        /* implementation d'un output stream dans lequel les données sont stockées dans un byte Array. 
	        */
	        ByteArrayOutputStream deserializationOutput = new ByteArrayOutputStream();
	        
	        
	        ImageStreamingDeserializer deserializer = new ImageDeserializerBase64StreamingImpl(deserializationOutput);
	        
	        
	        deserializer.deserialize(media);
	        
	      
	        byte[] deserializedImage = ((ByteArrayOutputStream)deserializer.getSourceOutputStream()).toByteArray();
	        
	        
	        // Vérification
	        // 1/ Automatique
	        
	     
	        byte[] originImage = Files.readAllBytes(image.toPath());
	        
	        //comparison
	        assert Arrays.equals(originImage, deserializedImage);
	        System.out.println("Cette sérialisation est bien réversible :)");

	        
	        //  2/ Manuelle
	        File extractedImage = new File("/Users/Oub/Desktop/foadNfa035Exercice1_2/src/foadNfa035Exercie1_2/petite_image_extraite.png");
	        new FileOutputStream(extractedImage).write(deserializedImage);
	        System.out.println("Je peux vérifier moi-même en ouvrant mon navigateur de fichiers et en ouvrant l'image extraite dans le répertoire de ce Test");

	    } catch (IOException e) {
	        e.printStackTrace();
	        }
	  
	    	}
	}

	//???????
	//Arrays.copyOfRange(originImage,0,originImage.length-2);

