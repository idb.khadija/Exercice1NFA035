package exercice1_2;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.Arrays;
import org.apache.commons.codec.binary.Base64;

	public abstract class AbstractStreamingImageSerializer<S,M> implements ImageStreamingSerializer<S,M> {
		  @Override
		  public void serialize(S source, M media) throws IOException {
		    getSourceInputStream(source).transferTo(getSerializingStream(media));
		  }
		}

