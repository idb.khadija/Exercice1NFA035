package exercice1_2;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import org.apache.commons.codec.binary.Base64OutputStream;
/*
 * La Classe ImageDeserializerBase64StreamingImpl est l'implementation de l'interface ImageStreamingDeserializer 
 * elle utilise la librairie org.apache.commons.codec.binary.Base64OutputStream pour decoder un flux et le retourner   
 */
@SuppressWarnings("rawtypes")
public class ImageDeserializerBase64StreamingImpl implements ImageStreamingDeserializer<ImageByteArrayFrame> {

	public ImageDeserializerBase64StreamingImpl(ByteArrayOutputStream deserializationOutput) {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void deserialize(Object media) throws IOException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public InputStream getDeserializingStream(Object media) throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public OutputStream getSourceOutputStream() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deserialize(ImageByteArrayFrame media) throws IOException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public <K extends InputStream> K getDeserializingStream(ImageByteArrayFrame media) throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

}
