package exercice1_2;

import java.io.IOException;

/*
 * La Classe abstraite AbstractStreamingImageDeserializer implemente l'interface ImageStreamingDeserializer*/
public abstract class AbstractStreamingImageDeserializer<M> implements ImageStreamingDeserializer<M> {
	    public final void deserialize(M media) throws IOException {
	        getDeserializingStream(media).transferTo(getSourceOutputStream());
	    }

}
     