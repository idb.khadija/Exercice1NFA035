package exercice1_2;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public interface ImageStreamingDeserializer<M> {
	  void deserialize(M media) throws IOException;
	  <K extends InputStream> K getDeserializingStream(M media) throws IOException;
	  <T extends OutputStream> T getSourceOutputStream();
	}