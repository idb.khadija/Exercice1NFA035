package exercice1_2;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public interface ImageStreamingSerializer<S, M>  {
	  void serialize(S source, M media) throws IOException;
	  <K extends InputStream> K getSourceInputStream(S source) throws IOException;
	  <T extends OutputStream> T getSerializingStream(M media) throws IOException;

	}

