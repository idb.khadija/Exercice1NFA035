package exercice1_2;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;


public abstract class AbstractImageFrameMedia<T> {

	private T channel;

	protected AbstractImageFrameMedia() {
	}
	public T getChannel() {
		return channel;
	}
	public void setChannel(T channel) {
		this.channel = channel;
	}
	AbstractImageFrameMedia(T channel){
		this.channel = channel;
	}
	public abstract OutputStream getEncodedImageOutput() throws IOException;
	public abstract InputStream getEncodedImageInput() throws IOException;
}


