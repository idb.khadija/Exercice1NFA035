package exercice1_2;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.commons.codec.binary.Base64OutputStream;
/*
 * Cette classe permet d'encoder un fichier en utilisant la librairie org.apache.commons.codec.binary.Base64OutputStream
 */  

public class ImageSerializerBase64StreamingImpl extends AbstractStreamingImageSerializer<File ,ImageByteArrayFrame>   {

	@SuppressWarnings("rawtypes")
	@Override
	public void serialize(File source, ImageByteArrayFrame media) throws IOException {
		try (FileInputStream fis = new FileInputStream(source)) {
			OutputStream encodedByteArray = new ByteArrayOutputStream();
			try (Base64OutputStream bos = new Base64OutputStream(encodedByteArray)) {
				int len = -1;
				byte[] buf = new byte[10000];
				while ((len = fis.read(buf)) > 0) {
					bos.write(buf, 0, len);
				}
				bos.flush();
			}
		}

	}

	@SuppressWarnings("unchecked")
	@Override
	public InputStream getSourceInputStream(File source) throws IOException {
		// TODO Auto-generated method stub
		
		
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public OutputStream getSerializingStream(ImageByteArrayFrame media) throws IOException {
		
		
		// TODO Auto-generated method stub
		return null;
	}
 
}
