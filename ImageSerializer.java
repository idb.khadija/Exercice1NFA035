package Exercice1NFA035;

import java.io.File;
import java.io.IOException;

/**
 *  interface ImageSerializer
 */
public interface ImageSerializer  {

	Object serialize(File image ) throws IOException;

	Object deserialize(String encodedImage) throws IOException, ClassNotFoundException;
	
	 

}
