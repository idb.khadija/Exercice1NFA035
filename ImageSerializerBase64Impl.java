package Exercice1NFA035;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
import java.util.*;

import javax.imageio.ImageIO;

/**
 *  la classe qui implémente l'interface ImageSerializer
 */

public class ImageSerializerBase64Impl implements ImageSerializer {
	
	


	
		public Object serialize(File image ) throws IOException {
			
			BufferedImage bufferedImage = ImageIO.read(image);
			WritableRaster raster = bufferedImage .getRaster();
			DataBufferByte data   = (DataBufferByte) raster.getDataBuffer();
			String encodedImage = java.util.Base64.getEncoder().encodeToString( data.getData());
			return encodedImage; }
	


		public  Object  deserialize( String encodedImage ) throws IOException, ClassNotFoundException {
			
			Object result = Base64.getDecoder().decode(encodedImage);
			return result;	
		}
		
}

	

