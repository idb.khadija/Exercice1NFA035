package Exercice1NFA035;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
/**
 * Méthode principale executéé
 */
public class Main {
	
	@SuppressWarnings("resource")
	public static void main(String[] args) {
        try {
            File image = new File("petite_image.png");
            ImageSerializer serializer = new ImageSerializerBase64Impl();

            // Sérialization
            String encodedImage = (String) serializer.serialize(image);
            System.out.println(splitDisplay(encodedImage,76));
          // System.out.print(encodedImage);
            // Désérialisation
            byte[] deserializedImage = null;
			try {
				deserializedImage = (byte[]) serializer.deserialize(encodedImage);
			} catch ( ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

            // Vérifications
            //  1/ Automatique
            assert (Arrays.equals(deserializedImage, Files.readAllBytes(image.toPath())));
            System.out.println("Cette sérialisation est bien réversible :)");
            //  2/ Manuelle
            File extractedImage = new File("petite_image_extraite.png");
            new FileOutputStream(extractedImage).write(deserializedImage);
            System.out.println("Je peux vérifier moi-même en ouvrant mon navigateur de fichiers et en ouvrant l'image extraite dans le répertoire de ce Test");


        } catch (IOException e) {
            e.printStackTrace();
        }
}

	private static String splitDisplay(String str, int i) {
		char[] charArray = 	str.toCharArray();
		String reconstructedString = null;
    
		for(int j=0; j<str.length(); j++) {
			reconstructedString = reconstructedString+charArray[j]; ;
		if (j%i==0) {
			reconstructedString = reconstructedString+charArray[j]+'\n';
		}
		
        
	}return reconstructedString; } }
